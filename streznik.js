var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});

app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});

/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje osebe)
 */
app.get('/api/dodaj', function(req, res) {
	// ...
	//res.send('Potrebno je implementirati dodajanje oseb!');
	// ...

	//var toAdd = req.query;
	//console.log(toAdd);
    uporabnikiSpomin.push(req.query);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje osebe)
 */
app.get('/api/brisi', function(req, res) {
	// ...
    console.log(req.query);

    for(var i in uporabnikiSpomin){
        var currDS = uporabnikiSpomin[i].ds;
        if(currDS == req.query.id){
            uporabnikiSpomin.splice(i, 1);
            //uporabnikiSpomin[i].
            res.send('ratal je');
            break;
        }
    }
    res.send('to pa ne gre');

	//res.send('Potrebno je implementirati brisanje oseb!');




	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{ds: '98765432', ime: 'James', priimek: 'Blond', ulica: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'},
	{ds: '12345678', ime: 'Ata', priimek: 'Smrk', ulica: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];